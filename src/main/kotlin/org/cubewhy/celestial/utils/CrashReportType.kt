package org.cubewhy.celestial.utils

enum class CrashReportType(@JvmField val jsonName: String) {
    LAUNCHER("launcher"),
    GAME("game")
}
